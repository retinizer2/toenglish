import os
import sys

ru = "абвгдежзиклмнопрстуфхцчшыэюя"

ru_full = "абвгдежзийклмнопрстуфхцчшщъыьэюя"

to_en =['a', 'b', 'v', 'g', 'd', 'e', 'zh', 'z', 'i', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'i', 'a' ,'yu', 'ia']


def show_var(name, v):
    print(name + " is: " + v)

try:
    path = sys.argv[1]
except:
    path = os.getcwd()

data = os.path.abspath(path)
show_var("data", data)

def to_english(char: str) -> str:
    in_ru = char in ru_full
    if in_ru:
        return to_en[ru.index(char)]
    elif in_ru and (char not in ru):
        return ''
    else:
        return char


        
def not_english(word: str) -> bool:
    for w in list(word):
        if w in ru_full:
            return True
    return False

def filenames(files: [str]):
    for f in files:
        src = os.path.join(data, f)
        new_name = ''.join(map(lambda x: to_english(x), f))
        dst = os.path.join(data, new_name)
        os.rename(src, dst)

        print(f + " -> " + new_name)


filenames(filter(lambda x: not_english(x),  os.listdir(data)))


